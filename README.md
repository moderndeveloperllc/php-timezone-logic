TZ Logic
=====

Very simple code snippet in PHP and jQuery to get the system list of timezones and create an array to be used for display. The javascript is used to pull the offset from the browser and set the select based on that and the list of defaults. The defaults are generated in PHP as DST can change the offsets. This is not perfect as some of the default cities might overlap offsets at various times of the year.

This is NOT intended to be production ready code. This is just a snippet to show how something can be done.