<!-- Controller code -->
<?php
foreach (DateTimeZone::listIdentifiers() as $tzName) {
    $timezone   = new DateTimeZone($tzName);
    $time       = new DateTime(null, $timezone);
    $offset     = $time->getOffset();
    $utctime    = clone $time;
    $utctime    = ($offset < 0) ? $utctime->sub(new DateInterval('PT' . abs($offset) . 'S'))
        : $utctime->add(new DateInterval('PT' . $offset . 'S'));
    $transition = $timezone->getTransitions($time->getTimestamp(), $time->getTimestamp());
    $abbr       = (!in_array(substr($transition[0]['abbr'], 0, 1), ["-", "+"])) ? $transition[0]['abbr'] . ' ' : '';
    $nameArray  = explode('/', $tzName);
    
    $friendly = (count($nameArray) > 2) ? array_pop($nameArray) . ', ' . array_pop($nameArray) : array_pop($nameArray);
    $friendly = str_replace('_', ' ', $friendly);

    $timezoneArray[$tzName]['value']  = $time->diff($utctime)->format($friendly . ' (' . $abbr . '%R%H:%I)');
    $timezoneArray[$tzName]['offset'] = ($offset / 60);
}

uasort($timezoneArray, function ($a, $b) {
    if ($a['offset'] == $b['offset']) {
        return strcasecmp($a['value'], $b['value']);
    }

    return ($a['offset'] < $b['offset']) ? -1 : 1;
});

//An opinionated list of the defaults for each time offset.
$jsDefaultArray = [
    $timezoneArray['Pacific/Midway']['offset']                 => 'Pacific/Midway',        //-11
    $timezoneArray['Pacific/Honolulu']['offset']               => 'Pacific/Honolulu',      //-10
    $timezoneArray['Pacific/Marquesas']['offset']              => 'Pacific/Marquesas',     //-9:30
    $timezoneArray['America/Anchorage']['offset']              => 'America/Anchorage',     //-9
    $timezoneArray['America/Los_Angeles']['offset']            => 'America/Los_Angeles',   //-8
    $timezoneArray['America/Denver']['offset']                 => 'America/Denver',        //-7
    $timezoneArray['America/Chicago']['offset']                => 'America/Chicago',       //-6
    $timezoneArray['America/New_York']['offset']               => 'America/New_York',      //-5
    $timezoneArray['America/Halifax']['offset']                => 'America/Halifax',       //-4
    $timezoneArray['America/St_Johns']['offset']               => 'America/St_Johns',      //-3:30
    $timezoneArray['America/Argentina/Buenos_Aires']['offset'] => 'America/America/Argentina/Buenos_Aires',    //-3
    $timezoneArray['America/Sao_Paulo']['offset']              => 'America/Sao_Paulo',     //-2
    $timezoneArray['Atlantic/Azores']['offset']                => 'Atlantic/Azores',       //-1
    $timezoneArray['Europe/London']['offset']                  => 'Europe/London',         //0
    $timezoneArray['Europe/Rome']['offset']                    => 'Europe/Rome',           //+1
    $timezoneArray['Europe/Athens']['offset']                  => 'Europe/Athens',         //+2
    $timezoneArray['Europe/Moscow']['offset']                  => 'Europe/Moscow',         //+3
    $timezoneArray['Asia/Tehran']['offset']                    => 'Asia/Tehran',           //+3:30
    $timezoneArray['Asia/Dubai']['offset']                     => 'Asia/Dubai',            //+4
    $timezoneArray['Asia/Kabul']['offset']                     => 'Asia/Kabul',            //+4:30
    $timezoneArray['Asia/Karachi']['offset']                   => 'Asia/Karachi',          //+5
    $timezoneArray['Asia/Kolkata']['offset']                   => 'Asia/Kolkata',          //+5:30
    $timezoneArray['Asia/Kathmandu']['offset']                 => 'Asia/Kathmandu',        //+5:45
    $timezoneArray['Indian/Chagos']['offset']                  => 'Indian/Chagos',         //+6
    $timezoneArray['Asia/Yangon']['offset']                    => 'Asia/Yangon',           //+6:30
    $timezoneArray['Asia/Jakarta']['offset']                   => 'Asia/Jakarta',          //+7
    $timezoneArray['Asia/Shanghai']['offset']                  => 'Asia/Shanghai',         //+8
    $timezoneArray['Asia/Pyongyang']['offset']                 => 'Asia/Pyongyang',        //+8:30
    $timezoneArray['Australia/Eucla']['offset']                => 'Australia/Eucla',       //+8:45
    $timezoneArray['Asia/Tokyo']['offset']                     => 'Asia/Tokyo',            //+9
    $timezoneArray['Australia/Darwin']['offset']               => 'Australia/Darwin',      //+9:30
    $timezoneArray['Australia/Brisbane']['offset']             => 'Australia/Brisbane',    //+10
    $timezoneArray['Australia/Adelaide']['offset']             => 'Australia/Adelaide',    //+10:30
    $timezoneArray['Australia/Sydney']['offset']               => 'Australia/Sydney',      //+11
    $timezoneArray['Pacific/Fiji']['offset']                   => 'Pacific/Fiji',          //+12
    $timezoneArray['Pacific/Auckland']['offset']               => 'Pacific/Auckland',      //+13
    $timezoneArray['Pacific/Chatham']['offset']                => 'Pacific/Chatham',       //+13:45
    $timezoneArray['Pacific/Kiritimati']['offset']             => 'Pacific/Kiritimati',    //+14
];
?>

<!-- View code -->

<!-- Display all system timezones -->
<select name="userTimezone" id="userTimezone" aria-label="Timezone">
    <option></option>
    <?php
    foreach ($timezoneArray as $key => $valArray) {
        echo '<option value="' . $key . '">' . $valArray['value'] . '</option>';
    }
    ?>
</select>

<!-- Place in footer of doc. This generates and sets the default TZ for user -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    //JS object containing the opinionated defaults for each offset
    var tzDefaults = {
        <?php
        foreach ($jsDefaultArray as $key => $value) {
            echo '"' . $key . '":"' . $value . '",
    ';
        }
        ?>};
</script>

<script type="text/javascript">
    //Set a timezone default based on the browser JS getTimezoneOffset()
    $(document).ready(function () {
        if (!$('#userTimezone option:selected').val()) {
            var current_date = new Date();
            var gmt_offset = -current_date.getTimezoneOffset();
            $('#userTimezone').val(tzDefaults[gmt_offset]);
        }
    });
</script>
